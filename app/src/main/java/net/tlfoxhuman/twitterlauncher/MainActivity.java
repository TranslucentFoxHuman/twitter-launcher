package net.tlfoxhuman.twitterlauncher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        Intent itt = getPackageManager().getLaunchIntentForPackage("com.twitter.android");
        if (itt != null) {
            startActivity(itt);
        } else {
            Toast.makeText(MainActivity.this, "Error: Cannnot launch twitter(X). Make sure you have the Twitter(X) Android app installed.", Toast.LENGTH_LONG).show();
        }
    }
}